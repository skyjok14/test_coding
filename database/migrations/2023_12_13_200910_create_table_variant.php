<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_variant', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->string('product_uid');
            $table->string('name');
            $table->integer('amount');
            $table->foreign(['product_uid'])->references(['uid'])->on('table_product')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_variant');
    }
};
