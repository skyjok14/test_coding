<?php

namespace App\Http\Controllers;

use App\Http\Controllers\helper\helperController;
use App\Models\productImage;
use App\Models\products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ProductsController extends Controller
{
    public function index()
    {
        try {
            $page = request()->input('page', 1);
            $limit = request()->input('limit', 10);
            $offset = ($page - 1) * $limit;

            $query = products::query();
            $countData = $query->count();
            $dbProduct = $query
                ->with('categoryProduct', 'image', 'variant')
                ->skip($offset)
                ->take($limit)
                ->get();

            $totalPage = ceil($countData / $limit);
            $data = [
                'currentPage' => $page,
                'limit' => $limit,
                'totalData' => $countData,
                'totalPage' => $totalPage,
            ];
            if ($countData === 0) {
                return response()->json(['message' => 'Data not found!'], 404);
            }

            return helperController::response($dbProduct, 'Success', 200, 'Get Data Success', $data);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Get Data Failed'], 500);
        }
    }

    public function store()
    {
        try {
            $validatedData = request()->validate([
                'name' => 'required',
                'category_uid' => 'required',
                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $input = [
                'uid' => helperController::getUid(),
                'category_uid' => $validatedData['category_uid'],
                'name' => $validatedData['name'],
            ];

           $product = products::create($input);

            $images = [];
            if (request()->hasFile('image')) {
                foreach (request()->file('image') as $image) {
                    $imageName = Str::random(40). '.' . $image->getClientOriginalExtension();
                    Storage::disk('image_product')->put($imageName, file_get_contents($image));
                    $imageData = [
                        'product_uid' => $product->uid,
                        'image' => $imageName,
                    ];
                    $images[] = $imageData;
                }
            }

            productImage::insert($images);

            return helperController::response(['product' => $input, 'images' => $images], 'Success', 201, 'Create Category Success', null);


        } catch (ValidationException $validationException) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validationException->errors(),
            ], 422);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Create failed'], 500);
        }
    }

    public function show(string $uid)
    {
        try {
            $dbProduct = products::where('uid', $uid)
                ->with('categoryProduct','image', 'variant')
                ->first();
            if (!$dbProduct) {
                return response()->json(['message' => 'Data not found'], 404);
            }
            return helperController::response($dbProduct, 'Success', 200, 'Get Data Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Get data failed'], 500);
        }
    }

    public function update(string $uid)
    {
        try {
            $dbProduct = products::where('uid', $uid)->first();

            if (!$dbProduct) {
                return response()->json(['message' => 'Data Not Found'], 404);
            }

            $validatedData = request()->validate([
                'name' => 'required',
                'category_uid' => 'required',
                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $dbProduct->update([
                'name' => $validatedData['name'],
                'category_uid' => $validatedData['category_uid'],
            ]);

            $updatedImages = [];
            if (request()->hasFile('image')) {
                foreach (request()->file('image') as $image) {
                    $imageName = Str::random(40). '.' . $image->getClientOriginalExtension();
                    Storage::disk('image_product')->put($imageName, file_get_contents($image));
                    $updatedImages[] = ['product_uid' => $dbProduct->uid, 'image' => $imageName];
                    // dump($image);
                }
            }
            // dd("test");
            productImage::insert($updatedImages);

            $allImages = $dbProduct->image()->select('id', 'image')->get();

            return helperController::response(['name' => $validatedData['name'], 'category_uid' => $validatedData['category_uid'], 'images' => $allImages], 'Success', 201, 'Update Data Success', null);
        } catch (ValidationException $validationException) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validationException->errors(),
            ], 422);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Update data failed'], 500);
        }
    }



    public function destroy(string $uid)
    {
        try {
            $dbProduct = products::where('uid', $uid)->first();
            if (!$dbProduct) {
                return response()->json(['message' => 'Data Not Found'], 404);
            }
            $dbProduct->delete();
            return helperController::response(null, 'Success', 201, 'Delete Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'delete failed'], 500);
        }
    }

    public function destroyImage(string $id){
        try {
            $dbImageProduct = productImage::find($id);
            if(!$dbImageProduct){
                return response()->json(['message' => 'Data Not Found'], 404);
            }
            $file = public_path('/assets/product_image/'. $dbImageProduct->image);
            if (file_exists($file)) {
                unlink($file);
            }
            $dbImageProduct->delete();
            return helperController::response(null, 'Success', 201, 'Delete Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'delete failed'], 500);
        }
    }
}
