<?php

namespace App\Http\Controllers\helper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class helperController extends Controller
{
    public static function getUid()
    {
        $bytes = random_bytes(10);
        $base64 = base64_encode($bytes);
        return rtrim(strtr($base64, '+/', '-_'), '-');
    }

    public static function response($result, $status, $statusCode, $message, $pagination){
        $resultPrint = [
            'status' => $status,
            'status_code' => $statusCode,
            'message' => $message ?? null,
            'data' => $result,
            'pagination' => $pagination ?? null
        ];
        return response()->json($resultPrint, $statusCode);
    }
}
