<?php

namespace App\Http\Controllers;

use App\Http\Controllers\helper\helperController;
use App\Models\CategoryProduct;
use Illuminate\Http\Request;

class CategoryProductController extends Controller
{
    public function index()
    {
        try {
            $page = request()->input('page', 1);
            $limit = request()->input('limit', 10);
            $offset = ($page - 1) * $limit;
            $query = CategoryProduct::query();
            $countData = $query->count();

            $dbCategory = $query
                ->skip($offset)
                ->take($limit)
                ->get();

            $totalPage = ceil($countData / $limit);

            $pagination = [
                'currentPage' => $page,
                'limit' => $limit,
                'totalData' => $countData,
                'totalPage' => $totalPage,
            ];

            if ($countData === 0) {
                return response()->json(['message' => 'Data Not Found'], 404);
            }

            return helperController::response($dbCategory, 'Success', 200, 'Get Data Category Success', $pagination);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Get Data Failed'], 500);
        }
    }

    public function store()
    {
        try {
            $name = request('name');
            if (!$name) {
                return response()->json(['message' => 'Please input Name'], 422);
            }
            
            $input = [
                'uid' => helperController::getUid(),
                'name' => $name,
            ];

            CategoryProduct::create($input);
            return helperController::response($input, 'Success', 201, 'Create Category Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Create Team failed'], 500);
        }
    }

    public function show(string $uid)
    {
        try {
            $dbCategory = CategoryProduct::where('uid', $uid)->first();
            if (!$dbCategory) {
                return response()->json(['message' => 'Data not found'], 404);
            }
            return helperController::response($dbCategory, 'Success', 200, 'Get Data Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Get data failed'], 500);
        }
    }

    public function update(string $uid){
        try{
            $name = request('name');
            $dbCategory = CategoryProduct::where('uid', $uid)->first();

            if(!$name){
                return response()->json(['message' => 'Please Input Name'], 422);
            }
            if(!$dbCategory){
                return response()->json(['message' => 'Data Not Found'], 404);
            }

            $dbCategory->update(['name' => $name]);

            return helperController::response(['name' => $name], 'Success', 201, 'Update Data Success', null);
        }catch(\Throwable $th){
            return response()->json(['error' => $th, 'Message' => 'Update data failed'], 500);
        }
    }

    public function destroy(string $uid){
        try{
            $dbCategory = CategoryProduct::where('uid', $uid)->first();
            if(!$dbCategory){
                return response()->json(['message' => 'Data Not Found'], 404);
            }
            $dbCategory->delete();
            return helperController::response(null, 'Success', 201, 'Delete Success', null);
        }catch(\Throwable $th){
            return response()->json(['error' => $th, 'message' => 'delete failed'], 500);
        }
    }
}
