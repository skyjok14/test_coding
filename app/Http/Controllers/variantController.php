<?php

namespace App\Http\Controllers;

use App\Http\Controllers\helper\helperController;
use App\Models\variants;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class variantController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $page = request()->input('page', 1);
            $limit = request()->input('limit', 10);
            $offset = ($page - 1) * $limit;
            $query = variants::query();
            $countData = $query->count();
            $dbVariant = $query->with('product')->skip($offset)->take($limit)->get();
            $totalPage = ceil($countData / $limit);
            $data = [
                'currentPage' => $page,
                'limit' => $limit,
                'totalData' => $countData,
                'totalPage' => $totalPage,
            ];
            if($countData === 0) {
                return response()->json(['message' => 'Data not found!'], 404);
            }
            return helperController::response($dbVariant, 'Success', 200, 'Get Data Success', $data);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Get Data Failed'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
     try {
        $validateData = request()->validate([
            'product_uid' => 'required',
            'name' => 'required',
            'amount' => 'required'
        ]);

        $input = [
            'uid' => helperController::getUid(),
            'product_uid' => $validateData['product_uid'],
            'name' => $validateData['name'],
            'amount' => $validateData['amount'],
        ];

        variants::create($input);
        return helperController::response($input,  'Success', 201, 'Create Variant Success', null);
     } catch (ValidationException $validationException) {
        return response()->json([
            'message' => 'Validation failed',
            'errors' => $validationException->errors(),
        ], 422);
    } catch (\Throwable $th) {
        return response()->json(['error' => $th, 'message' => 'Create failed'], 500);
     }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $uid)
    {
        try {
            $dbVariant = variants::where('uid', $uid)->with('product')->first();
            if(!$dbVariant){
                return response()->json(['message' => 'Data not found'], 404);
            }
            return helperController::response($dbVariant, 'success', 200, 'Get Data Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Get data failed'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(string $uid)
    {
        try {
            $dbVariant = variants::where('uid', $uid)->first();
            if (!$dbVariant) {
                return response()->json(['message' => 'Data Not Found'], 404);
            }
            $validateData = request()->validate([
                'product_uid' => 'required',
                'name' => 'required',
                'amount' => 'required'
            ]);
            $dbVariant->update([
                'product_uid' => $validateData['product_uid'],
                'name' => $validateData['name'],
                'amount' => $validateData['amount'],
            ]);
            return helperController::response($validateData, 'Success', 201, 'Create Variant Success', null);
        }  catch (ValidationException $validationException) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validationException->errors(),
            ], 422);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'Update data failed'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $uid)
    {
        try {
            $dbVariant = variants::where('uid', $uid)->first();
            if(!$dbVariant){
                return response()->json(['message' => 'Data Not Found'], 404);
            }
            $dbVariant->delete();
            return helperController::response(null, 'Success', 201, 'Delete Success', null);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th, 'message' => 'delete failed'], 500);
        }
    }
}
