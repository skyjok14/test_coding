<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productImage extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'table_product_image';
    public $timestamps = true;


    public function product()
    {
        return $this->belongsTo(products::class, 'product_uid', 'uid');
    }
}
