<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table ='table_product';
    public $timestamps = true;


    public function getRouteKeyName()
    {
        return 'uid';
    }

    public function categoryProduct(){
        return $this->belongsTo(CategoryProduct::class, 'category_uid', 'uid')->select('uid', 'name');
    }

    public function image(){
        return $this->hasMany(ProductImage::class, 'product_uid', 'uid');
    }

    public function variant(){
        return $this->hasMany(variants::class, 'product_uid', 'uid');
    }
}
