<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'table_product_categories';
    public $timestamps = true;

    public function getRouteKeyName()
    {
        return 'uid';
    }
}
