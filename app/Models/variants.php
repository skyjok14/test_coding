<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class variants extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'table_variant';
    public $timestamps = true;

    public function getRouteKeyName()
    {
        return 'uid';
    }

    public function product(){
        return $this->belongsTo(products::class, 'product_uid', 'uid')->select('uid', 'name', 'category_uid');
    }
}
