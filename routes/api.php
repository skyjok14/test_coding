<?php

use App\Http\Controllers\CategoryProductController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\variantController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
// Route::resource('/products'. ProductsController::class);

Route::resource('/category-product', CategoryProductController::class);
Route::resource('/product',ProductsController::class);
Route::delete('/delete-image/{id}', [ProductsController::class, 'destroyImage']);
Route::resource('/variant-product', variantController::class);
