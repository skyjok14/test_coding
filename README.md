
## Dokumentasi Test Coding Backend menggunakan laravel 10

Panduan menggunakan clone test 

- git clone https://github.com/username/repo.git
- cd repo
- composer install
- salin .env.example
- buat baru `.env` dan tempelkan disini masukan env `DB_DATABASE=test_coding`
- aktivkan xampp atau laragon 
- php artisan key:generate
- php artisan migrate
- php artisan serve


## HIT API
# Api Category 
- http://127.0.0.1:8000/api/category-product   `Get` 
- http://127.0.0.1:8000/api/category-product/{uid}=   `Get Detail` 
- http://127.0.0.1:8000/api/category-product   `Post` key -> name
- http://127.0.0.1:8000/api/category-product/{uid}   `Post` key -> name  tambahkan key dan value `_method : put` untuk melakukan update/put
- http://127.0.0.1:8000/api/category-product/{uid}  <-- gunakan uid seperti disamping  `Delete`   


# Api Product 
- http://127.0.0.1:8000/api/product  `Get` 
- http://127.0.0.1:8000/api/product/{uid} `Get Detail` 
- http://127.0.0.1:8000/api/product  `Post` key -> category_uid, name, image[]
- http://127.0.0.1:8000/api/product/{uid}   `Post` key -> category_uid, name, image[] tambahkan key dan value `_method : put` untuk melakukan update/put
- http://127.0.0.1:8000/api/product/{uid}   `Delete`
- http://127.0.0.1:8000/api/delete-image/{id} `Delete Image`

# Api Variant
- http://127.0.0.1:8000/api/variant-product `Get`
- http://127.0.0.1:8000/api/variant-product/{uid} `Get Detail`
- http://127.0.0.1:8000/api/variant-product `Post ` key -> product_uid,  name, amount
- http://127.0.0.1:8000/api/variant-product/{uid} `Post` key -> product_uid,  name, amount tambahkan key dan value `_method : put` untuk melakukan update/put
- http://127.0.0.1:8000/api/variant-product/{uid} `Delete`


